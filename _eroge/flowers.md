---
    title:
      en: "Flowers -Le Volume sur Printemps-"
    vndb: 14267
    difficulty: 20
    first_exp: 75
    focus: "characters"
    age_rating:
      - 0
    vocab:
      - "Oujousama-talk"
      - "school life"
    rating_reason:
      - "While the keigo/honorifics may confuse you, nothing the characters talk about will confuse you much."
    pitfalls:
      - "They speak in a way you usually don’t see in other VNs as in very formal talk."
      - "Has some unhookable dialog."
    recommended:
      - "One of the few yuri VNs that is not a just porn based or a light hearted character driven story to make you feel all fuzzy on the inside and has is pretty decent."
---
