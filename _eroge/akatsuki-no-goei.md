---
    title:
      en: "Akatsuki no Goei"
      jp: "暁の護衛"
    vndb: 629
    difficulty: 60
    first_exp: 0
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "bodyguards"
    rating_reason:
      - "It’s not easy as some other VNs but the sentences are still straight to the point. The plot is non-existent so it’s pretty much impossible to get lost. The plot eventually comes up in the 3rd part of the trilogy, but you don’t have to worry about that on this."
    pitfalls:
      - "The writer pretty much wrote this for the jokes more than the actual plot. As a result, unless you know what they are saying 100%, the jokes will make no sense and it will be quite boring. Watching manzai humor on youtube may help."
    recommended:
      - "It’s a good comedy VN and later in the trilogy it gets into some serious material."
---
