---
    title:
      en: "Aiyoku no Eustia"
      jp: "穢翼のユースティア"
    vndb: 3770
    difficulty: 50
    first_exp: 100
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "everyday life"
      - "religion"
      - "fantasy"
    rating_reason:
      - "Straightforward sentences structures and plot. Plot is still fun enough to keep the reader interested until the very end. Nothing too elaborate compared to most plot-oriented VNs with short sentences so it’s easy to follow."
    pitfalls:
      - "The vocabulary will be difficult to get used to at first due to the fantasy element and unique vocabulary that comes with such settings, so expect to move at a snail's pace for chapter 1. Occasionally there may be some grammar that may be difficult to look up at times."
    recommended:
      - "Extremely fascinating setting that will keep you hooked until the end. By the end of this game, you will be familiar with 敬語 whether you like it or not."
---
