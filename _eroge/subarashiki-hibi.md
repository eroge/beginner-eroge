---
    title:
      en: "Subarashiki Hibi ~Furenzoku Sonzai~"
      jp: "素晴らしき日々～不連続存在～"
    vndb: 3144
    difficulty: 50
    first_exp: 0
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "everyday life"
      - "school life"
      - "philosophy"
      - "religion"
      - "mathematics"
      - "science"
    rating_reason:
      - "The difficult parts are noticeable but seldom such as the part at the end of Rabbit Hole 1. Other than that, interactions with Ayana will make you double-take."
    pitfalls:
      - "The sentences themselves are pretty straightforward in that there isn’t much grammatical complexity, however you must be 100% sure what you are reading to fully understand what the author is trying to convey. There are some parts that are in fact challenging but they’re relatively short Combine this with the fact that it is the prose and the atmosphere that wins this VN praise for being a kamige, makes it difficult for beginners to fully appreciate this VN. If you read this as a beginner, make sure to re-read it at a later time."
    recommended:
      - "It is one of the best games that messes with your perception of what is happening because it all connects very well and has a clear purpose. There is an incredible array of characters and is extremely thought provoking. It can’t be really simply summarized into a genre and is unlike anything else out there."
---
