---
    title:
      en: "Hatsuyuki Sakura"
      jp: "はつゆきさくら"
    vndb: 7302
    difficulty: 40
    first_exp: 50
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "supernatural"
      - "bunnies"
    rating_reason:
      - "While it may get harder to read in the later parts, the beginning is quite easy."
    pitfalls:
      - "The true route may not be enjoyable if you have to look up every word."
    recommended:
      - "Considered one of the best Nakige (which means it will probably make you cry) out there. If you like these in general, you should plan to read this sooner or later."
---
