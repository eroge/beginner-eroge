---
    title:
      en: "Parfait ~Chocolat Second Brew~"
      jp: "パルフェ ～ショコラ second brew～"
    vndb: 683
    difficulty: 80
    first_exp: 0
    age_rating:
      - 12
      - 18
    focus: "characters"
    vocab:
      - "cooking"
      - "business"
      - "everyday Life"
    rating_reason:
      - "It comes off as a normal character based game at first and you will be able to follow everything for a *while*. Emphasis on the word *while*. Do not be fooled into thinking you will be able to understand everything as time goes on if you pick this as your first VN."
    pitfalls:
      - "While it is a character based game, the prose can get complex further into the VN. Also unlike most VNs where it’s set in high school, this is set in college and in a restaurant setting so you maybe not be able to recognize some of the common words associated with those. The characters also don’t fall into your usual trope-filled characters so don’t expect them to stick with short sentences. This is definitely a good example of how Japanese people would actually speak."
    recommended:
      - "The writer of the game, Maruto, is a very good one."
---
