---
    title:
      en: "Hoshi Ori Yume Mirai"
      jp: "星織ユメミライ"
    vndb: 14265
    difficulty: 40
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      - "school life"
    rating_reason:
      - "It’s a dating sim where the only purpose if for the MC to find a girl so it is easy to follow. Lots of CGs to provide cues to the plot if you are lost."
    pitfalls:
      - "It’s very long if you want to finish the entire VN."
    recommended:
      - "If you want a VN that goes into long term relationship, this is one of the more famous ones."
---
