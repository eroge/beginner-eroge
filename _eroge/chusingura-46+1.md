---
    title:
      en: "ChuSinGura 46+1"
      jp: "忠臣蔵46+1"
    vndb: 12260
    difficulty: 60
    first_exp: 0
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "samurai/shogunate/bushido"
      - "old language"
    rating_reason:
      - "Yes it’s set in the 1800’s but the MC is from the modern age and with him providing the narrative and lot of the dialogue is centered around him, it is still possible to still read this VN."
    pitfalls:
      - "However, just because you can doesn’t mean it’s a good idea to read this as your first VN. Some of the conversations from other characters will seem even more foreign to you than normal and some words will not even be in modern dictionaries as they are archaic. The VN is also very long and takes time for the plot to start so be ready for that."
    recommended:
      - "It is a unique take on the story of chusingura and is a prime example of a doujin VN done right."
---
