---
    title:
      en: "Daitoshokan no Hitsujikai"
      jp: "大図書館の羊飼い"
    vndb: 8158
    difficulty: 60
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      - "school life"
    rating_reason:
      - "A standard slice of life character based game where you follow characters around in their school. More difficult than many other standard character driven games but there isn’t too much of a plot for one to get lost in until the true route."
    pitfalls:
      - "Much more difficult than your standard character based game since the characters actually do stuff."
    recommended:
      - "It’s fairly good slice of life VN with fun characters. The cat, Gizaemon, is pretty awesome too."
---
