---
    title:
      en: "Harumade, Kururu."
      jp: "はるまで、くるる。"
    vndb: 9198
    difficulty: 60
    first_exp: 15
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "sex terms"
      - "school life"
    rating_reason:
      - "Very straight forward like all the other VNs that share its rating.......kind of."
    pitfalls:
      - "Be ready for never ending collection of H-scenes in the very beginning. The game becomes more interesting once you manage to overcome those, so try not to drop it before then."
      - "(The h-scenes in question are hilarious, though)"
      - "There are some hard parts later on in the game, it would be a good idea to find somebody (who has finished the game), that you can ask questions to when you reach those."
    recommended:
      - "Good sci-fi VN even if it may have some rough edges."
---
