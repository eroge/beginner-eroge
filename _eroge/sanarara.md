---
    title:
      en: "Sanarara"
      jp: "サナララ"
    vndb: 1510
    difficulty: 35
    first_exp: 90
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      - "school life"
      - "scifi"
    rating_reason:
      - "The VN is actually collection of 7 different stories with each story only about 3-5 hours long. The conversations between the characters are short and simple with the narration also being easy to follow. The scifi element is explained in very easy terms"
    pitfalls:
      - "Despite looking like a regular nakige, there is actually scifi element to it which could be confusing for some people."
    recommended:
      - "Genuinely good VN that doesn’t try to be more than what it actually is, which is making the reader sad. The navigator system is also used in very interesting ways"
---
