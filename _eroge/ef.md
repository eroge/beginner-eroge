---
    title:
      en: "ef - a fairy tale of the two."
    vndb: 88
    difficulty: 40
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      - "school life"
    rating_reason:
      - "Another very easy to read VN in Japanese. This one also has the \"benefit\" of having an anime adaptation so you should have no problem following the plot."
    pitfalls:
      - "You have no control over how fast you can read the VN since you do not have control over the text in some parts."
    recommended:
      - "It’s a very solid romance VN that is bound to leave some people crying by the end."
---
