---
    title:
      en: "fortissimo EXS//Akkord:nächsten Phase"
      jp: "フォルテシモ エグゼス ネクステン ファーゼ"
    vndb: 7595
    difficulty: 70
    first_exp: 10
    age_rating:
      - 15
      - 18
    focus: "plot"
    vocab:
      - "school life"
      - "magic"
      - "fighting"
      - "chuu2"
    rating_reason:
      - "The game in general, tends to be fairly descriptive and a tad bit artistic with the author’s way of putting things together. There’s a tendency to usually cram lots of information in long sentences and have the reader work around several ideas when abilities are being introduced (he’ll talk about what the ability entitles, how impossible it’ll be to overcome it, what would happen ‘even if’ something happened, show how the ability gets beaten, and more explanations on how the interactions occurred). The story is not difficult to follow, but it is easy to get lost in the fights if you aren’t paying attention. With that said, it’s completely manageable provided that you’ve read a few VNs first."
    pitfalls:
      - "The game has long sentences, relatively high vocabulary including 四字熟語 and uncommon expressions."
      - "There’s some less common grammar used, such as んとする、んばかり、らしからぬ、せん and some slang among the characters (most notable in the beginning with somebody who talks in Kansai ben)"
    recommended:
      - "It’s a fantastic introduction to the world of chuuni that isn’t overwhelming difficult but remains challenging enough for you to get used to the expressions and vocabulary of more notable works (Dies Irae, Fate Stay Night, Comyu)."
---
