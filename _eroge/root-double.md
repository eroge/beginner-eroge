---
    title:
      en: "Root Double -Before Crime * After Days-"
      jp: "ルートダブル -Before Crime * After Days-"
    vndb: 5000
    difficulty: 40
    first_exp: 100
    age_rating:
      - 15
    focus: "characters"
    vocab:
      - "biology"
      - "physics"
      - "science"
    rating_reason:
      - "Short lines without much out of the ordinary writing wise and a plot that doesn’t make itself hard to follow."
      - "It uses many specialized terms, but despite that, it’s still very manageable."
    pitfalls:
      - "The infodumps might not be very hard to read, but there are many of them, especially near the end of the game. Might make it tough to get through those parts."
      - "The in game terms dictionary (TIPS) can’t be texthooked, however, you can freely read it whenever you like to and the vocab is the same as that that normally appears in the game, so if you’re having trouble you can always retry later when you’ve improved a bit."
    recommended:
      - "It’s for the most part a fun ride. if you like the zero escape games you’d probably enjoy this"
    hookcode:
      - ["/HSN-C@BE00!319577E4"]
---
