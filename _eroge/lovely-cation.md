---
    title:
      en: "Lovely x Cation"
      jp: "ＬＯＶＥＬＹ×Ｃ∧ＴＩＯＮ"
    difficulty: 30
    vndb: 6682
    first_exp: 75
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "daily life"
      - "dating"
    rating_reason:
      - "It’s a dating sim where the only purpose if for the mc to find a girl so it is easy to follow. Lots of CGs to provide cues to the plot if you are lost."
    pitfalls:
      - "Depends on whether you like dating sims or not and willing to learn the mechanics of the games."
    recommended:
      - "More offering for the waifu wars? There is really no other reason to read this other than to date 2D girls."
---
