---
    title:
      en: "Hoshizora no Memoria -Wish upon a Shooting Star-"
      jp: "星空のメモリア-Wish upon a shooting star-"
    vndb: 1474
    difficulty: 40
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      - "school life"
      - "astronomy"
    rating_reason:
      - "This is a no brainer as anyone who has read it knows that there isn’t much complicated material in the VN. The characters speak in short concise sentences and while there some astronomy terms used, it shouldn’t stump most beginners."
    pitfalls:
      - "Some of the supernatural elements may confuse some people."
    recommended:
      - "It's an ok starter VN as well as being easy to read in Japanese. The true route is the highlight of the VN so expect to slog through some routes."
---
