---
    title:
      en: "Tokeijikake no Ley Line -Tasogaredoki no Kyoukaisen-"
      jp: "時計仕掛けのレイライン　－黄昏時の境界線－ル"
    vndb: 10016
    difficulty: 40
    first_exp: 25
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "everyday life"
      - "magic"
    rating_reason:
      - "Very straightforward plot and very simple sentences. It does have mystery aspect to it, but it should be easy for even beginners to figure stuff out."
    pitfalls:
      - "It is a trilogy and the best bit won’t happen until the 2nd part. There are some bits about magic that may stump beginners"
    recommended:
      - "Good cast and good plot that is short."
---
