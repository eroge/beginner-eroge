---
    title:
      en: "Twinkle ☆ Crusaders"
      jp: "ティンクル☆くるせいだーす"
    vndb: 666
    difficulty: 40
    first_exp: 10
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "nonsense"
      - "magic"
      - "school life"
    rating_reason:
      - "The actual sentences themselves are not anything out of the ordinary nor are the vocabulary that difficult."
    pitfalls:
      - "For about the first 30 minutes, nothing will make sense because the writer seems to be insane. After that is calms down but some of the characters speak in a non-sensical manner."
    recommended:
      - "It’s fun to read crazy things and this certainly is one. Don’t expect much from the gameplay though."
    hookcode:
      - ["/HBN-4@463D76"]
      - ["/HSN2C@463D65"]
---
