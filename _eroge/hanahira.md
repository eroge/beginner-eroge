---
    title:
      en: "Hanahira!"
      jp: "はなひらっ！"
    vndb: 5244
    difficulty: 10
    first_exp: 100
    age_rating:
      - 0
    focus: "characters"
    vocab:
      - "school life"
      - "everyday life"
    rating_reason:
      - "Almost no kanji and there isn’t much of need for them either. The story is non-existent with no depth. Perfect choice for a throwaway practice VN. The sentences are short with little grammatical depth and to the point with no flair in its prose. It is also quite short. This is about as easy as a VN can get unless you can find VNs aimed at children."
    pitfalls:
      - "It might bore you so much you get put off from learning the language forever."
    recommended:
      - "If you insist that you MUST have a throw away VN and do not want to be overwhelmed by kanji, then this is about as easy as it can get. There is no story so I cannot recommend this to anyone who just wants a good VN to read."
---
