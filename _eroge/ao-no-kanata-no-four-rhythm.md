---
    title:
      en: "Ao no Kanata no Four Rhythm"
      jp: "蒼の彼方のフォーリズム"
    vndb: 12849
    difficulty: 50
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "school life"
      - "everyday life"
      - "flight"
    rating_reason:
      - "It does devote a lot of time on preparing and taking part in the flying circus, but it’s not as elaborate as one may think. It does have lot of SoL parts in the script which are comparable to other character based games out there."
    pitfalls:
      - "The flying circus and the narrative that comes with it may confuse some people at first. But after a few times I think it should be ok."
    recommended:
      - "A good well paced VN with something futuristic sport added in to keep you interested. There is also the anime so if you liked that, you should read this."
---
