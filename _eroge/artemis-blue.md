---
    title:
      en: "Artemis Blue"
      jp: "アルテミスブルー"
    vndb: 5451
    difficulty: 50
    first_exp: 100
    age_rating:
      - 18 #i can't find the rumored all ages version from the previous google docs tho tbh
    focus: "plot"
    vocab:
      - "engineering"
    rating_reason:
      - "Straight forward dialogues in standard Japanese. Because all the characters are all adults, they don’t use weird speech patterns you would normally see in anime. The plot does get complicated but it should be relatively easy to follow as it is straightforward. Many of the infodumps have technical words which are translated very well into English."
    pitfalls:
      - "Even though individual technical words may translate well, put in an actual sentence they may confuse some people."
    recommended:
      - "While not the best VN you can read, it’s a good introduction to a more realistic scifi VNs. Also one of the few VNs with a single mom MC."
---
