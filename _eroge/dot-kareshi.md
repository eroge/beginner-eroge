---
    title:
      en: "Dot Kareshi -We're 8bit Lovers-"
      jp: "ドットカレシ-We're 8bit Lovers-"
    vndb: 12158
    difficulty: 40
    first_exp: 100
    age_rating:
      - 15
    focus: "characters"
    vocab:
      - "RPG"
    rating_reason:
      - "This is a relatively easy VN to read and good for practice. In addition it’s split into 3 parts so even if you don’t like it, it’s short like Hanahira."
    pitfalls:
      - "The MC likes to speak in emojis for some reason and you may get rightfully annoyed."
    recommended:
      - "It is rather an interesting concept to have the VN set in an MMO game."
---
