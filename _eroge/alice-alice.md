---
    title:
      en: "ALICE=ALICE"
      jp: "アリス＝アリス"
    vndb: 13100
    difficulty: 40
    first_exp: 75
    age_rating:
      - 15
    focus: "characters"
    vocab:
      - "alice in wonderland references"
    rating_reason:
      - "While the original Lewis Carroll’s Novel may be strangely written, this interpretation of Alice in wonderland is rather easy to read especially if you read the original novel or watched the disney movie."
    pitfalls:
      - "Some characters like the chesire neko speaks strangely and that may confuse people"
    recommended:
      - "It is pretty short and the plot should be clear to most people already so if you can get past some of the weird speaking patterns, it should be ok."
---
