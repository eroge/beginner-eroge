---
    title:
      en: "Flyable Heart -The future has already begun-"
    vndb: 1179
    difficulty: 40
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      -  "school life"
    rating_reason:
      - "It’s a standard light hearted character based game often used as a throw-away practice VN. It actually has more kanji unlike Hanahira but like all of these kinds of stories, there is little plot and easy to follow the plot."
    pitfalls:
      - "Most people don’t finish it because it’s not very good."
    recommended:
      - "It’s a very easy to read VN. Also lets you play the much better [alternative story](https://vndb.org/v3789)"
---
