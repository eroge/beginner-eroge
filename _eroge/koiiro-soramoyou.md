---
    title:
      en: "Koiiro Soramoyou"
      jp: "恋色空模様"
    vndb: 1740
    difficulty: 20
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "slice of life"
      - "school life"
    rating_reason:
      - "Because of the floating boxes, it seems that there is a limitation for how much the characters can speak as all the characters spoke even more concisely than usual."
    pitfalls:
      - "They speak in a way you usually don’t see in other VNs."
    recommended:
      - "Floating text box is nice?"
---
