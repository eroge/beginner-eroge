---
    title:
      en: "Little Busters!"
      jp: "リトルバスターズ！"
    vndb: 5
    difficulty: 40
    first_exp: 100
    age_rating:
      - 0
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      - "school life"
    rating_reason:
      - "The prose in Key VNs are quite simple so it shouldn’t be too difficult to understand what each lines says."
    pitfalls:
      - "The downside is that during the emotional scenes, the impact may be lessened if you try to look up several words every line."
      - "One of the minigames has voice only parts."
    recommended:
      - "One of the best well known key VNs. While it’s not the best nakige out there, it’s still not bad."
---
