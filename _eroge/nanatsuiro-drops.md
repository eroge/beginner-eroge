---
    title:
      en: "Nanatsuiro★Drops"
      jp: "ななついろ★ドロップス"
    vndb: 193
    difficulty: 40
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "everyday life"
      - "school life"
      - "magic"
    rating_reason:
      - "The protagonist is voiced whenever he is in his stuffed animal form. It has the benefit of having an anime adaption, if you have trouble with some of the universe specific terms and the setting, then watching episode 1 of the anime should help."
    pitfalls:
      - "The intro of the game can’t be hooked."
      - "There is however, a transcript that can be found [here](http://pastebin.com/x2LnCjVa)."
      - "Also has an auto progressing and unhookable segment near the end of the game (unfocusing the window pauses the game so you will probably be able to manage)."
    recommended:
      - "The VN is divided into chapters and it might help you feeling like you're making progress. Recommended if you like the main heroine (Akihime Sumomo) and want to read a pure love story."
    hookcode:
      - ["/HBC@43FE50","Before patch"]
      - ["/HBC@43FCC0","After patch"]
---
