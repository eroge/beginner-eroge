---
    title:
      en: "Evenicle"
      jp: "イブニクル"
    vndb: 16640
    difficulty: 60
    first_exp: 75
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "fantasy"
    rating_reason:
      - "About as easy as you could get for a VN with gameplay. The story is easy to follow and you will never be confused about MC’s motivation for doing things."
    pitfalls:
      - "The fantasy aspect may be too much for some. The gameplay aspect is also unhookable so you may have trouble figuring out what different abilities do. Also expect lots of H-scenes."
    recommended:
      - "The gameplay is genuinely fun and the story is not too bad either. Most people will find the gameplay to be too easy though."
---
