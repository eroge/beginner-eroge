---
    title:
      en: "Tsuki ni Yorisou Otome no Sahou"
      jp: "月に寄りそう乙女の作法"
    vndb: 10680
    difficulty: 90
    first_exp: 0
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "high society"
      - "school life"
      - "fashion"
      - "clothes making"
      - "maid life"
    rating_reason:
      - "At it’s core, it is a character based game with a huge fan base. However, you should never use this as your first VN as most of the humor comes from the puns and bad Japanese from foreigners. If you don’t know correct Japanese, you will be confused as to why a certain Swiss character’s speech is funny."
      - "Furthermore, the lines in this vn can get very long, which could be challenging to tackle."
    pitfalls:
      - "Luna’s kanji puns, Ursule and Sasha’s strange speech patterns, Aeon’s almost chuuni like speaking pattern, Nanai’s outbursts, quite possibly Minato’s accent. Then there is Ou Jackson’s prologue. This is about as difficult a character based game will get."
    recommended:
      - "It has some of the quirkiest characters you can see in VNs and the MC is probably one of the most interesting MCs you will ever see. The relationship between Asahi and Luna is exceptional in how natural it feels."
---
