---
    title:
      en: "Kimi to Kanojo to Kanojo no Koi."
      jp: "君と彼女と彼女の恋。"
    vndb: 7738
    difficulty: 30
    first_exp: 25
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "everyday life"
    rating_reason:
      - "Has short sentences and is pretty easy grammar-wise."
      - "Vocab shouldn't be much of a problem either, because most of the time you'll be seeing just the usual daily life conversations."
    pitfalls:
      - "There are a few points at which character will be speaking without the text box on the screen. While the lines itself are simple enough, this might still be confusing for some."
      - "There is a character that speaks only in kana, which could be tricky to deal with."
    recommended:
      - "First of all, the game is quite short, so it's a good choice for anyone who doesn't want to invest a huge amount of time into reading a single VN. Also, totono is a pretty (in)famous nitro+ game, so it's worth reading anyway. It might not really be that good and some people don't like it, but the main idea is still quite unique and you won't lose much of your time whether you like it or not."
    hookcode:
      - ["/ha-8@48d456"]
---
