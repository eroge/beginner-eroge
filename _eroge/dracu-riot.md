---
    title:
      en: "Dracu-Riot!"
      jp: "ドラクリオット"
    vndb: 8213
    difficulty: 40
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "slice of life"
    rating_reason:
      - "It's mostly a character based game, it's easy to tell what's going on from the context and the sentences are short."
    pitfalls:
      - "Some of the infodumps might be a bit hard to understand and it get slightly more difficult toward the end of some of the routes."
    recommended:
      - "The cast is enjoyable and if you like SoL; it's a decent story."
---
