---
    title:
      en: "Soshite Ashita no Sekai yori"
      jp: "そして明日の世界より――"
    vndb: 420
    difficulty: 20
    first_exp: 50
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "school life"
      - "everyday life"
      - "news"
    rating_reason:
      - "While the premise of the VN is depressing, the actual focus is on continuing their everyday life as best as possible. Hence you won’t see anything more difficult than your usual character based game."
    pitfalls:
      - "You may not like the fact that everyone will die from the impending apocalypse. And no, that is not spoilers, that is the premise of the VN"
    recommended:
      - "One of the few VNs dealing with the end of the world. While it may sound like an utsuge, it’s not as depressing as you may think. It’s a nice break from the usual high school based VNs."
---
