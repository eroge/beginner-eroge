---
    title:
      en: "Clover Days"
    vndb: 13325
    difficulty: 40
    first_exp: 100
    age_rating:
      - 18
    focus: "characters"
    vocab:
      - "kansai-ben"
      - "everyday life"
    vocab_notes: "kansai-ben is only for one character"
    rating_reason:
      - "It’s a standard cute light hearted character game.."
    pitfalls:
      - "The kansai-ben aspect may stump people."
    recommended:
      - "You may have a kansai-ben fetish and this will absolutely satisfy on that front. It is a solid cute story so if you want that while reading something actually worthwhile, this is it."
---
