---
    title:
      en: "Baldr Sky"
      jp: "バルドスカイ"
    vndb: 1306
    difficulty: 50
    first_exp: 100
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "mechs"
      - "computing"
      - "cyberpunk"
    rating_reason:
      - "Everything is straightforward from the dialogue to the info-dumps."
    pitfalls:
      - "Has tons of tutorial material to read that is displayed as image, which cannot be text hooked."
      - "Is also extremely long at more than 4mb of text. You must also learn the gameplay mechanics to get to finish the VN."
    recommended:
      - "It’s incredible and you will gladly put in 100+ hours."
---
