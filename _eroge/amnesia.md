---
    title:
      en: "Amnesia"
      jp: "アムネシア"
    vndb: 7803
    difficulty: 40
    first_exp: 100
    age_rating:
      - 15
    focus: "characters"
    vocab:
      - "everyday life"
    rating_reason:
      - "Without any narrative, it becomes very easy to read. The conversations themselves shouldn’t be too difficult to handle as most of the conversation is around everyday life stuff."
    pitfalls:
      - "This is one of the very few VNs you can’t really go wrong with as the narration is usually the most difficult part to read and this has none."
    recommended:
      - "Very good looking bishies. The fact that it has no narration and the MC almost never speaks is strange but very solid otome VN."
    hookcode:
      - ["","Requires you to hook a 32bit version of ppsspp with ITHVNR or VNR"]
---
