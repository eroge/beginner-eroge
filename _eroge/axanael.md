---
    title:
      en: "Axanael"
      jp: "アザナエル"
    vndb: 3971
    difficulty: 12
    first_exp: 100
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "everyday life"
    rating_reason:
      - "Has short sentences and is pretty easy grammar-wise. Narration is almost non-existent and the game is fully voiced, so it's like Hanahira, but with an actual plot. Obviously, more difficult, but still a lot more easy than most of the games in this list."
    pitfalls:
      - "Some characters speak in unusual manner, so it may be confusing. The game itself isn't very popular, so it may be not for everyone (mostly thanks to Shimokura's sense of humor)."
    recommended:
      - "Again, the game is quite short. This is a perfect choice if you feel that you're ready for something more plot driven. If the humor and characters don't put you off, you'll get a pretty fun game."
    hookcode:
      - ["/HA-8@48DE10", "needs version 1.10 of the game"]
---
