---
    title:
      en: "Dekinai Watashi ga, Kurikaesu."
      jp: "できない私が、くり返す。"
    vndb: 15166
    difficulty: 50
    first_exp: 30
    age_rating:
      - 18
    focus: "plot"
    vocab:
      - "everyday life"
      - "random trivia"
    rating_reason:
      - "The game's difficulty varies depending on how well you know your standard Kanji, and whether or not if you can understand the tricky jokes."
    pitfalls:
      - "Has a lot of comedy,  so one may find this repetitive and overbearing. The \"meat\" of the VN is in Shino's route and the True route.  Comparing these with the other character routes is like night and day. May have pacing issues."
    recommended:
      - "Likeable characters, meaningful message. A roller coaster of feelings."
---
